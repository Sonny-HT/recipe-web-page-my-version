# Generated by Django 4.1.7 on 2023-02-24 15:57

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Recipe',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200)),
                ('author', models.CharField(max_length=100)),
                ('picture', models.URLField(null=True)),
                ('description', models.TextField()),
                ('created_on', models.DateField(auto_now_add=True)),
            ],
        ),
    ]
