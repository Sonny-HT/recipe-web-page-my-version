from django.shortcuts import render,get_object_or_404, redirect
from recipes.models import Recipe
from recipes.forms import RecipeForm

def recipe_homepage(request):
    recipe_list = Recipe.objects.all()
    content = {
        'recipe_list':recipe_list
    }
    return render(request,'main.html',content)

def get_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    content = {
        'recipe' : recipe
    }
    return render(request,'details.html',content)


def create_recipe(request): #create a new function that takes a request as an argument
    if request.method == 'POST': #this if statement checks if a form has been submitted
        form = RecipeForm(request.POST) #assigns a variable to the class created in forms.py
        if form.is_valid(): #checks if all instances of the forms meets the requirements outlined in the models.py file
            form.save() #saves the form to submit into the database
            return redirect('recipe_homepage') #redirect brings you back to another page
    else: #if the form is not valid
        form = RecipeForm() #creates the form object from forms.py again and assigns it to variable fuckyou
    context = {
        'form': form #variable assigned to a key named 'form' to be called by html
    }
    return render(request,'create.html', context) #renders the template with the context of form


def edit_recipe(request,id):
    recipe = get_object_or_404(Recipe, id=id)
    if request.method == 'POST':
        form = RecipeForm(request.POST, instance=recipe) #the instance=Recipe lets django know to edit an existing recipe
        if form.is_valid():
            form.save()
            return redirect('get_recipe', id)
    else:
        form = RecipeForm(instance=recipe)

    content = {
        'form' : form,
        'recipe':recipe
    }
    return render(request,'edit.html', content)
