from django.db import models

class Recipe(models.Model):
    title = models.CharField(max_length=200)
    author = models.CharField(max_length=100)
    picture = models.URLField(blank=True)
    description = models.TextField()
    created_on = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.title

class Step(models.Model):
    instruction = models.TextField()
    step_number = models.SmallIntegerField()
    recipe = models.ForeignKey('Recipe', related_name='steps',on_delete=models.CASCADE)

    class Meta:
        ordering = ['step_number']
