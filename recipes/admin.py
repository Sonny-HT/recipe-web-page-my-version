from django.contrib import admin
from .models import Recipe,Step

# Register your models here.
@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'title',
        'author',
        'picture',
        'description',
        'created_on',
    )

@admin.register(Step)
class StepAdmin(admin.ModelAdmin):
    list_display = (
        'step_number',
        'instruction',
        'id'
    )
