from django.urls import path
from .views import recipe_homepage, get_recipe,create_recipe,edit_recipe

urlpatterns = [
    path('', recipe_homepage, name = 'homepage'),
    path('recipe/<int:id>', get_recipe, name ='get_recipe'),
    path('create_recipe/create', create_recipe, name='create_recipe'),
    path('recipe/<int:id>/edit', edit_recipe, name='edit_recipe')
]
