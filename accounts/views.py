from django.shortcuts import render,redirect
from django.contrib.auth import login,authenticate,logout
from django.contrib.auth.models import User
from accounts.forms import SignUpForm, LoginForm

# Create your views here.

def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            password_confirmation = form.cleaned_data['password_confirmation']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            if password == password_confirmation:
                user = User(username=username,password=password,first_name=first_name,last_name=last_name)
                login(request, user)
                return redirect('homepage')
            else:
                form.add_error('password_confirmation', "Passwords do not match")
    else:
        form = SignUpForm()
    context= {
        'form':form,
    }
    return render(request, 'accounts/signup.html',context)


def signin(request):
    if request.method=='POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(request,username=username,password=password)
            if user is not None:
                login(request, user)
                return redirect('homepage')
            else:
                form.add_error(None, 'Invalid username or password')
    else:
        form = LoginForm()
    context = {
        'form': form,
    }
    return render(request,'accounts/login.html',context)


def signout(request):
    logout(request)
    return redirect('homepage')
