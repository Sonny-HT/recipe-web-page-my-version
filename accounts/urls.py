from django.urls import path
from accounts.views import signup,signin,signout

urlpatterns = [
    path('login/', signin, name='login'),
    path('signup/', signup, name='signup'),
    path('logout/', signout, name='logout')
]
